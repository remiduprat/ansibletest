# Ansible

### Configuration ###

* Modifier le fichier ansible.cgf (sous /etc/ansible/ ) <br>
  `vault_password_file = pass.yml` <br><br>
  
* Dans pass.yml, s'assurer d'avoir la ligne ansible_sudo_pass: _mot de passe_ <br><br>

Les applications et utilisateurs par machines sont définies par deux Array, correspondant à 
l'hôte lançant la task.

_pas obligatoire_
* Modifier le fichier Playbook site.yml <br>
    `vars:
    ansible_password: "{{ ansible_sudo_pass }}"` <br><br>

* Installer posix
  ``ansible-galaxy collection install ansible.posix``
  
* Installer collections aws 
  ``ansible-galaxy collection install amazon.aws``
  ``ansible-galaxy collection install community.aw``

### Lancer ansible ###

_Remplacer \#host\# par l'hôte ciblé (serveurs_prod pour tous) et #task pour le fichier cibled_

Ajouter --ask-vault-pass et @pass.yml dans les --extra-vars pour utiliser un mot de passe pour l'utilisateur incarné, comme avec pctest

`ansible-playbook -i . --extra-vars "target=#host#" #task#.yml`
